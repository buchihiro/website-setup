#!/bin/bash

sudo yum install docker git -y
sudo service docker start

mkdir /home/ec2-user/bin/
curl -L https://github.com/docker/compose/releases/download/1.13.0/docker-compose-`uname -s`-`uname -m` > /home/ec2-user/bin/docker-compose
sudo chmod +x /home/ec2-user/bin/docker-compose


git clone https://buchihiro@bitbucket.org/buchihiro/website.git /home/ec2-user/website

cd /home/ec2-user/website

sudo docker info
sudo docker build -t nginx_web01 .
sudo /home/ec2-user/bin/docker-compose up